document.addEventListener('DOMContentLoaded', () => {
    const movieTitleInput = document.getElementById('movieTitle');
    const movieInfoContainer = document.getElementById('movieInfoContainer');
    const movieTitleDisplay = document.getElementById('movieTitleDisplay');
    const movieYear = document.getElementById('movieYear');
    const movieActors = document.getElementById('movieActors');
    const moviePlot = document.getElementById('moviePlot');
    const moviePoster = document.getElementById('moviePoster');
  
    function buscarPelicula() {
      const inputTitle = movieTitleInput.value;
  
      // Validar si el título está vacío
      if (!inputTitle) {
        alert('Por favor, introduzca el título de la película.');
        return;
      }
  
      // Consumir el API con el título proporcionado
      fetch(`https://www.omdbapi.com/?t=${inputTitle}&plot=full&apikey=30063268`)
        .then(response => response.json())
        .then(data => {
          // Verificar si el título introducido coincide exactamente con el título de la película
          if (data.Title && data.Title.toLowerCase() === inputTitle.toLowerCase()) {
            mostrarInformacionPelicula(data);
          } else {
            alert('No se encontró una película con el título proporcionado.');
          }
        })
        .catch(error => console.error('Error al obtener la información de la película:', error));
    }
  
    function mostrarInformacionPelicula(movieData) {
      movieTitleDisplay.textContent = movieData.Title;
      movieYear.textContent = movieData.Year;
      movieActors.textContent = movieData.Actors;
      moviePlot.textContent = movieData.Plot;
      moviePoster.src = movieData.Poster;
  
      // Mostrar el contenedor de información de la película
      movieInfoContainer.style.display = 'block';
    }
  
    function limpiar() {
      // Limpiar el contenido y ocultar el contenedor de información de la película
      movieTitleInput.value = '';
      movieInfoContainer.style.display = 'none';
    }
  
    // Asociar funciones a eventos de los botones
    document.querySelector('button:nth-of-type(1)').addEventListener('click', buscarPelicula);
    document.querySelector('button:nth-of-type(2)').addEventListener('click', limpiar);
  });
  